document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

var sCount = 0;
var windowW = 0

$('document').ready(function () {
    windowW = $(window).width();
    console.log(windowW);
});

window.onresize = function () {
    windowW = $(window).width();
    console.log(windowW);
}

function clickNext() {
    if (sCount > 0) {
        sCount--;
        sPos = sCount * 100;
        $(".row-slider").css("margin-left", '-' + sPos + '%');
        console.log(sCount);
    }
}

function clickPref() {
    if (sCount < 3) {
        sCount++;
        sPos = sCount * 100;
        $(".row-slider").css("margin-left", '-' + sPos + '%');
        console.log(sCount);
    }
}

var xDown = null;
var yDown = null;

function getTouches(evt) {
    return evt.touches || // browser API
        evt.originalEvent.touches; // jQuery
}

function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];
    xDown = firstTouch.clientX;
    yDown = firstTouch.clientY;
};

function handleTouchMove(evt) {
    if (!xDown || !yDown) {
        return;
    }

    var xUp = evt.touches[0].clientX;
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if (windowW < 768) {
        if (Math.abs(xDiff) > Math.abs(yDiff)) {
            /*most significant*/
            if (xDiff > 0) {
                if (sCount < 3) {
                    sCount++;
                    sPos = sCount * 100;
                    $(".row-slider").css("margin-left", '-' + sPos + '%');
                    console.log(sCount);
                }
            } else {
                if (sCount > 0) {
                    sCount--;
                    sPos = sCount * 100;
                    $(".row-slider").css("margin-left", '-' + sPos + '%');
                    console.log(sCount);
                }
            }
        } else {
            if (yDiff > 0) {
                /* up swipe */
            } else {
                /* down swipe */
            }
        }
    }
    /* reset values */
    xDown = null;
    yDown = null;
};